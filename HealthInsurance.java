/**
 * 
 */

/**
 * @author Animesh Dutta
 *
 */
public class HealthInsurance {
	
	public String Name="Norman Gomes";
	public String Gender="Male";
    public int Age=34;
    public String CurrentHealth_Hypertension="No";
    public String CurrentHealth_BloodPressure="No";
    public String CurrentHealth_BloodSugar="No";
    public String CurrentHealth_Overweight="Yes";
    public String Habits_Smoking="No";
    public String Habits_Alcohol="Yes";
    public String Habits_DailyExercise="Yes";
    public String Habits_Drugs="No";
    
   	public static void main(String[] args) {
   		System.out.println("Health Insurance Premium for Mr. Gomes:Rs 6,856");
   		
	}

}
